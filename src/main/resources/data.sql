INSERT INTO authorities VALUES(1, 'ADMIN');
INSERT INTO authorities VALUES(2, 'TRAINER');
INSERT INTO authorities VALUES(3, 'CLIENT');

INSERT INTO rooms VALUES(1, 15, 'GREEN');
INSERT INTO rooms VALUES(2, 60, 'BLUE');
INSERT INTO rooms VALUES(3, 25, 'YELLOW');
INSERT INTO rooms VALUES(4, 40, 'RED');

INSERT INTO activities VALUES(1, 'ABS');
INSERT INTO activities VALUES(2, 'Box');
INSERT INTO activities VALUES(3, 'CrossFit');
INSERT INTO activities VALUES(4, 'Joga');
INSERT INTO activities VALUES(5, 'Judo');
INSERT INTO activities VALUES(6, 'Kalistenika');
INSERT INTO activities VALUES(7, 'Kettlebells');
INSERT INTO activities VALUES(8, 'Kickboxing');
INSERT INTO activities VALUES(9, 'Stretching');
INSERT INTO activities VALUES(10, 'Tabata');
INSERT INTO activities VALUES(11, 'Zumba');
