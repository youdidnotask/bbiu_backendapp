package pl.dmcs.bbiu.back.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@Data
public class UserForm {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;
}
