package pl.dmcs.bbiu.back.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@Data
public class ScheduleForm {

    @NotNull
    private long roomId;

    @NotNull
    private long activityId;

    @NotNull
    private long trainerId;

    @NotBlank
    private String day;

    @NotBlank
    private String time;
}
