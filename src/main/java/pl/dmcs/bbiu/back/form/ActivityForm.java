package pl.dmcs.bbiu.back.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@Data
public class ActivityForm {

    @NotNull
    private Long userId;

    @NotNull
    private Long activityId;
}
