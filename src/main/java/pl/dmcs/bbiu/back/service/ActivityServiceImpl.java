package pl.dmcs.bbiu.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.model.Activity;
import pl.dmcs.bbiu.back.repository.ActivityRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public Optional<Activity> get(long id) {
        return activityRepository.findById(id);
    }

    @Override
    public List<Activity> get() {
        return activityRepository.findAll();
    }
}
