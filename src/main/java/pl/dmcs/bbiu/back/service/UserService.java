package pl.dmcs.bbiu.back.service;

import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.wrapper.ActivityWrapper;
import pl.dmcs.bbiu.back.wrapper.UserWrapper;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 20/06/2020
 */
public interface UserService {
    Boolean exists(String username);
    User addUser(String username, String password, Role role);
    Optional<User> get(long id);
    Optional<User> get(String username);
    List<UserWrapper> getUsers(Role role);
    UserWrapper getUserDetails(long id);
    List<ActivityWrapper> assignActivity(long userId, long activityId);
    void createTestUsers();
}
