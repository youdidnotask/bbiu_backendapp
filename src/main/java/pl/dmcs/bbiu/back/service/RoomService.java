package pl.dmcs.bbiu.back.service;

import pl.dmcs.bbiu.back.model.Room;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
public interface RoomService {
    Optional<Room> get(long id);
    List<Room> get();
}
