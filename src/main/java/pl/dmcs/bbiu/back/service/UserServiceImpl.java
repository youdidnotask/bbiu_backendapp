package pl.dmcs.bbiu.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.model.Activity;
import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.repository.UserRepository;
import pl.dmcs.bbiu.back.wrapper.ActivityWrapper;
import pl.dmcs.bbiu.back.wrapper.UserWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 20/06/2020
 */
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    public Boolean exists(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public User addUser(String username, String password, Role role) {
        Authority authority = authorityService.find(role);
        User user = new User(username, passwordEncoder.encode(password), authority);
        userRepository.save(user);
        return user;
    }

    @Override
    public Optional<User> get(long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> get(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<UserWrapper> getUsers(Role role) {
        return UserWrapper.wrap(userRepository.findAllByAuthoritiesRole(role));
    }

    @Override
    public UserWrapper getUserDetails(long id) {
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()) {
            return UserWrapper.wrap(user.get());
        }
        return null;
    }

    @Override
    public List<ActivityWrapper> assignActivity(long userId, long activityId) {
        User user = userRepository.findById(userId).get();
        Activity activity = activityService.get(activityId).get();
        user.addActivity(activity);
        userRepository.save(user);
        return ActivityWrapper.wrap(user.getActivities());
    }


    @Override
    public void createTestUsers() {
        List<User> testUserList = new ArrayList<>();

        Authority trainerAuthority = authorityService.find(Role.TRAINER);
        User trainerUser = new User("trainer", passwordEncoder.encode("pass"));
        trainerUser.addAuthority(trainerAuthority);
        testUserList.add(trainerUser);

        Authority clientAuthority = authorityService.find(Role.CLIENT);
        User clientUser = new User("client", passwordEncoder.encode("pass"));
        clientUser.addAuthority(clientAuthority);
        testUserList.add(clientUser);

        userRepository.saveAll(testUserList);
    }
}
