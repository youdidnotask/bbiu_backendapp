package pl.dmcs.bbiu.back.service;

import pl.dmcs.bbiu.back.model.Activity;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
public interface ActivityService {
    Optional<Activity> get(long id);
    List<Activity> get();
}
