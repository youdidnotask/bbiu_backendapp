package pl.dmcs.bbiu.back.service;

import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
public interface AuthorityService {
    Authority find(Role role);

    static List<String> processAuthorities(Set<Authority> authorities) {
        List<String> authorityList = new ArrayList<>();
        authorities.forEach(authority -> authorityList.add(authority.getAuthority()));
        return authorityList;
    }
}
