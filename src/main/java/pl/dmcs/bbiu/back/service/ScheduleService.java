package pl.dmcs.bbiu.back.service;

import pl.dmcs.bbiu.back.form.ScheduleForm;
import pl.dmcs.bbiu.back.model.Schedule;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
public interface ScheduleService {
    String KEY_RESULT = "result";
    String KEY_SCHEDULE = "scheduleList";
    String KEY_MY_SCHEDULE = "mySchedule";

    Schedule add(Schedule schedule);
    Schedule add(ScheduleForm form);
    List<Schedule> get();
    Map<String, Object> signIn(String username, long scheduleId);
    Map<String, Object> signOut(String username, long scheduleId);
}
