package pl.dmcs.bbiu.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.exception.AppInternalException;
import pl.dmcs.bbiu.back.exception.ErrorCode;
import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.repository.AuthorityRepository;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public Authority find(Role role) {
        return authorityRepository.findByRole(role).orElseThrow(() -> new AppInternalException(ErrorCode.RESOURCE_NOT_FOUND));
    }
}
