package pl.dmcs.bbiu.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.model.Room;
import pl.dmcs.bbiu.back.repository.RoomRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;


    @Override
    public Optional<Room> get(long id) {
        return roomRepository.findById(id);
    }

    @Override
    public List<Room> get() {
        return roomRepository.findAll();
    }
}
