package pl.dmcs.bbiu.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.form.ScheduleForm;
import pl.dmcs.bbiu.back.model.Activity;
import pl.dmcs.bbiu.back.model.Room;
import pl.dmcs.bbiu.back.model.Schedule;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.repository.ScheduleRepository;
import pl.dmcs.bbiu.back.wrapper.ScheduleWrapper;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private UserService userService;

    @Override
    public Schedule add(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    @Override
    @Transactional
    public Schedule add(ScheduleForm form) {
        Room room = roomService.get(form.getRoomId()).get();
        Activity activity = activityService.get(form.getActivityId()).get();
        User trainer = userService.get(form.getTrainerId()).get();
        DayOfWeek dayOfWeek = DayOfWeek.of(Integer.valueOf(form.getDay()));
        LocalTime time = LocalTime.parse(form.getTime());

        Schedule schedule = new Schedule();
        schedule.setRoom(room);
        schedule.setActivity(activity);
        schedule.setTrainer(trainer);
        schedule.setDay(dayOfWeek);
        schedule.setTime(time);
        return scheduleRepository.save(schedule);
    }

    @Override
    public List<Schedule> get() {
        return scheduleRepository.findAllByOrderByDayAscTimeAsc();
    }

    @Override
    @Transactional
    public Map<String, Object> signIn(String username, long scheduleId) {
        User client = userService.get(username).get();
        Schedule schedule = scheduleRepository.getOne(scheduleId);
        Boolean result = schedule.addClient(client);
        if (result) {
            scheduleRepository.save(schedule);
        }
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(KEY_RESULT, result);
        responseMap.put(KEY_MY_SCHEDULE, ScheduleWrapper.extractScheduleIds(client));
        return responseMap;
    }

    @Override
    @Transactional
    public Map<String, Object> signOut(String username, long scheduleId) {
        User client = userService.get(username).get();
        Schedule schedule = scheduleRepository.getOne(scheduleId);
        Boolean result = schedule.removeClient(client);
        if (result) {
            scheduleRepository.save(schedule);
        }
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(KEY_RESULT, result);
        responseMap.put(KEY_MY_SCHEDULE, ScheduleWrapper.extractScheduleIds(client));
        return responseMap;
    }
}
