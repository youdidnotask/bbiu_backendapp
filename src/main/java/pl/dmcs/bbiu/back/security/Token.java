package pl.dmcs.bbiu.back.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Token {
    private Long id;
    private String username;
    private List<String> scope;
    private String token;

    public Token(String username, List<String> scope, String token) {
        this.username = username;
        this.scope = scope;
        this.token = token;
    }
}
