package pl.dmcs.bbiu.back.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.service.AuthorityService;

import java.util.*;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@Service
public class JwtServiceImpl implements JwtService {

    @Value("${api.security.key}")
    private String apiKey;

    @Value("${api.security.token-duration}")
    private Long duration;

    @Override
    public Token generateTokenAdmin(UserDetails userDetails) {
        String username = userDetails.getUsername();
        List<String> scope = Arrays.asList(Role.ADMIN.getName());

        Map<String, Object> userData = new HashMap<String, Object>();
        userData.put(KEY_USER, username);
        userData.put(KEY_SCOPE, scope);

        return new Token(username, scope, buildToken(username, userData));
    }

    public Token generateToken(User user) {
        Long id = user.getId();
        String username = user.getUsername();
        List<String> scope = AuthorityService.processAuthorities(user.getAuthorities());

        Map<String, Object> userData = new HashMap<String, Object>();
        userData.put(KEY_ID, id);
        userData.put(KEY_USER, username);
        userData.put(KEY_SCOPE, scope);

        return new Token(id, username, scope, buildToken(username, userData));
    }

    public Claims parseToken(String token) {
        return Jwts.parser().setSigningKey(apiKey).parseClaimsJws(token).getBody();
    }

    private String buildToken(String username, Map<String, Object> userData) {
        return Jwts.builder()
                .setSubject(username)
                .setClaims(userData)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + duration))
                .signWith(SignatureAlgorithm.HS256, apiKey)
                .compact();

    }
}
