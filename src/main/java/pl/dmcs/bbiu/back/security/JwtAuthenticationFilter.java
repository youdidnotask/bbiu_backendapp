package pl.dmcs.bbiu.back.security;

import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.dmcs.bbiu.back.controller.TestController;
import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.service.UserServiceImpl;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by Joanna Olejnik
 * 22/06/2020
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION_TYPE_BEARER = "Bearer";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserServiceImpl userService;

    @Value("${app.admin.login}")
    private String adminLogin;

    @Value("${app.admin.pass}")
    private String adminPass;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(SecurityContextHolder.getContext().getAuthentication() == null) {
            String header = request.getHeader(HEADER_AUTHORIZATION);
            if (header != null && StringUtils.hasText(header) && header.startsWith(AUTHORIZATION_TYPE_BEARER)) {
                String token = header.substring(AUTHORIZATION_TYPE_BEARER.length());
                try {
                    Claims claims = jwtService.parseToken(token);
                    String username = (String) claims.get(JwtService.KEY_USER);
                    UserDetails user;
                    if (adminLogin != null && adminLogin.equalsIgnoreCase(username)) {
                        user = new User(adminLogin, adminPass);
                    } else {
                        user = userService.loadUserByUsername(username);
                    }
                    List<String> authorities = (List<String>) claims.get(JwtService.KEY_SCOPE);
                    Set<SimpleGrantedAuthority> grantedAuthorities = buildGrantedAuthorities(authorities);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    private Set<SimpleGrantedAuthority> buildGrantedAuthorities(List<String> authorities) {
        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = new HashSet<>();
        authorities.forEach(authority -> simpleGrantedAuthorities.add(new SimpleGrantedAuthority(authority)));
        return simpleGrantedAuthorities;
    }
}
