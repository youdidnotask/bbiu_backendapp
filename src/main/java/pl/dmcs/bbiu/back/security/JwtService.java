package pl.dmcs.bbiu.back.security;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;
import pl.dmcs.bbiu.back.model.User;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
public interface JwtService {
    String KEY_ID = "id";
    String KEY_USER = "user";
    String KEY_SCOPE = "scope";

    Token generateTokenAdmin(UserDetails userDetails);
    Token generateToken(User user);
    Claims parseToken(String token);
}
