package pl.dmcs.bbiu.back.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.dmcs.bbiu.back.model.Activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@Data
@AllArgsConstructor
public class ActivityWrapper {
    public long id;
    public String name;
    public List<UserWrapper> trainers;

    public ActivityWrapper(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public static List<ActivityWrapper> wrap(Collection<Activity> activities) {
        List<ActivityWrapper> activityWrapperList = new ArrayList<>();
        for (Activity activity : activities) {
            activityWrapperList.add(new ActivityWrapper(activity.getId(), activity.getName(), UserWrapper.wrap(activity.getTrainers())));
        }
        return activityWrapperList;
    }
}
