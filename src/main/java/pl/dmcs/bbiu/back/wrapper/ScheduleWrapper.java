package pl.dmcs.bbiu.back.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.dmcs.bbiu.back.model.Schedule;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.service.ScheduleService;

import java.util.*;

/**
 * Created by Joanna Olejnik
 * 13/07/2020
 */
@Data
@AllArgsConstructor
public class ScheduleWrapper {
    public long id;
    public String room;
    public String activity;
    public String trainer;
    public String day;
    public String time;
    public int participants;
    public int capacity;

    public static List<ScheduleWrapper> wrap(Collection<Schedule> schedules) {
        List<ScheduleWrapper> scheduleWrapperList = new ArrayList<>();
        for (Schedule schedule : schedules) {
            scheduleWrapperList.add(new ScheduleWrapper(
                    schedule.getId(),
                    schedule.getRoom().getName(),
                    schedule.getActivity().getName(),
                    schedule.getTrainer().getUsername(),
                    schedule.getDay().name(),
                    schedule.getTime().toString(),
                    schedule.getClients().size(),
                    schedule.getRoom().getCapacity()
            ));
        }
        return scheduleWrapperList;
    }

    public static Set<Long> extractScheduleIds(User user) {
        Set<Long> scheduleIds = new HashSet<>();
        for (Schedule schedule : user.getSchedule()) {
            scheduleIds.add(schedule.getId());
        }
        return scheduleIds;
    }

    public static Map<String, Object> prepareRespone(Collection<Schedule> schedules, Optional<User> user) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(ScheduleService.KEY_SCHEDULE, wrap(schedules));
        if (user.isPresent()) {
            responseMap.put(ScheduleService.KEY_MY_SCHEDULE, extractScheduleIds(user.get()));
        }
        return responseMap;
    }
}
