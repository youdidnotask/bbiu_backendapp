package pl.dmcs.bbiu.back.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.service.AuthorityService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@Data
@AllArgsConstructor
public class UserWrapper {
    public long id;
    public String email;
    public boolean enabled;
    public List<String> scope;
    public List<ActivityWrapper> activities;
    public List<ScheduleWrapper> schedule;

    public UserWrapper(long id, String email, boolean enabled) {
        this.id = id;
        this.email = email;
        this.enabled = enabled;
    }

    public static List<UserWrapper> wrap(Collection<User> users) {
        List<UserWrapper> userWrapperList = new ArrayList<>();
        for (User user : users) {
            userWrapperList.add(new UserWrapper(user.getId(), user.getUsername(), user.isEnabled()));
        }
        return userWrapperList;
    }

    public static UserWrapper wrap(User user) {
        return new UserWrapper(
                user.getId(),
                user.getUsername(),
                user.isEnabled(),
                AuthorityService.processAuthorities(user.getAuthorities()),
                ActivityWrapper.wrap(user.getActivities()),
                ScheduleWrapper.wrap(user.getSchedule()));
    }
}
