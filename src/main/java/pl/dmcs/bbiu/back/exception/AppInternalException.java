package pl.dmcs.bbiu.back.exception;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
public class AppInternalException extends RuntimeException {
    private final ErrorCode errorCode;

    public AppInternalException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return this.errorCode.getMessage();
    }
}
