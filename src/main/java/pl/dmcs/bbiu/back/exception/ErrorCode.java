package pl.dmcs.bbiu.back.exception;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
public enum ErrorCode {
    RESOURCE_NOT_FOUND("Resource not found");

    private final String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
