package pl.dmcs.bbiu.back.util;

/**
 * Created by Joanna Olejnik
 * 20/06/2020
 */
public class Constants {
    public static final String AUTHORITY_ADMIN = "ROLE_ADMIN";
    public static final String AUTHORITY_TRAINER = "ROLE_TRAINER";
    public static final String AUTHORITY_CLIENT = "ROLE_CLIENT";

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_TRAINER = "TRAINER";
    public static final String ROLE_CLIENT = "CLIENT";

    public static final String ERROR_USER_EXISTS = "User already exists.";
}
