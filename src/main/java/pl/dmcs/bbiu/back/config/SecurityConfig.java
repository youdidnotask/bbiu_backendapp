package pl.dmcs.bbiu.back.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.dmcs.bbiu.back.security.JwtAuthenticationFilter;
import pl.dmcs.bbiu.back.service.UserServiceImpl;
import pl.dmcs.bbiu.back.util.Constants;

/**
 * Created by Joanna Olejnik
 * 20/06/2020
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserServiceImpl userService;

    @Value("${app.admin.login}")
    private String adminLogin;

    @Value("${app.admin.pass}")
    private String adminPass;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilterBean() {
        return new JwtAuthenticationFilter();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(adminLogin)
                .password(passwordEncoder().encode(adminPass))
                .roles(Constants.ROLE_ADMIN)
                .authorities(Constants.AUTHORITY_ADMIN);

        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                        .antMatchers("/api/security/register-employee").hasRole(Constants.ROLE_ADMIN)
                        .antMatchers("/api/users/employees/activity").hasAnyRole(Constants.ROLE_ADMIN, Constants.ROLE_TRAINER)
                        .antMatchers("/api/users/employees").hasRole(Constants.ROLE_ADMIN)
                        .antMatchers("/api/users/clients").hasRole(Constants.ROLE_ADMIN)
                        .antMatchers("/api/users/account").authenticated()
                        .anyRequest().permitAll();

        http.addFilterBefore(jwtAuthenticationFilterBean(), UsernamePasswordAuthenticationFilter.class);
    }
}
