package pl.dmcs.bbiu.back.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 19/06/2020
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
@NamedEntityGraph(name = "User.Details",
    attributeNodes = {
        @NamedAttributeNode("activities"),
        @NamedAttributeNode("schedule")
})
public class User implements UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @JsonIgnore
    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name = "user_authority",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authorities = new HashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable( name = "trainer_activity",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "activity_id"))
    private Set<Activity> activities = new HashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable( name = "client_schedule",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "schedule_id"))
    private Set<Schedule> schedule = new HashSet<>();

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.enabled = true;
    }

    public User(String username, String password, Authority authority) {
        this.username = username;
        this.password = password;
        this.addAuthority(authority);
        this.enabled = true;
    }

    public User(String username, String password, Set<Authority> authorities) {
        this.username = username;
        this.password = password;
        for(Authority authority : authorities) {
            this.addAuthority(authority);
        }
        this.enabled = true;
    }

    public void addAuthority(Authority authority) {
        authority.getUsers().add(this);
        this.authorities.add(authority);
    }

    public void addActivity(Activity activity) {
        activity.getTrainers().add(this);
        this.activities.add(activity);
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
