package pl.dmcs.bbiu.back.model;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dmcs.bbiu.back.controller.TestController;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Getter
@Setter
@Entity
@Table(name = "schedule")
public class Schedule {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false)
    private Room room;

    @ManyToOne
    @JoinColumn(name = "activity_id", nullable = false)
    private Activity activity;

    @ManyToOne
    @JoinColumn(name = "trainer_id", nullable = false)
    private User trainer;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private DayOfWeek day;

    @Column(nullable = false)
    private LocalTime time;

    @ManyToMany(mappedBy = "schedule")
    private Set<User> clients = new HashSet<>();

    public boolean addClient(User client) {
        if (this.clients.size() < this.room.getCapacity()) {
            client.getSchedule().add(this);
            this.getClients().add(client);
            return true;
        }
        return false;
    }

    public boolean removeClient(User client) {
        if (this.clients.contains(client)) {
            client.getSchedule().remove(this);
            this.clients.remove(client);
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return id.equals(schedule.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
