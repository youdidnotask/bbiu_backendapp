package pl.dmcs.bbiu.back.model;

/**
 * Created by Joanna Olejnik
 * 19/06/2020
 */
public enum Role {
    ADMIN,
    TRAINER,
    CLIENT;

    private final String PREFIX = "ROLE_";

    public String getName() {
        return PREFIX + this.name();
    }
}
