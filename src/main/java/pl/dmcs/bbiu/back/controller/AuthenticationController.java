package pl.dmcs.bbiu.back.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.dmcs.bbiu.back.form.UserForm;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.security.JwtService;
import pl.dmcs.bbiu.back.service.UserService;
import pl.dmcs.bbiu.back.util.Constants;

import javax.validation.Valid;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@RestController
@RequestMapping("/api/security")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Value("${app.admin.login}")
    private String adminLogin;

    @PostMapping(value = "/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserForm form) {
        if (userService.exists(form.getUsername())) {
            return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(Constants.ERROR_USER_EXISTS);
        }
        User user = userService.addUser(form.getUsername(), form.getPassword(), Role.CLIENT);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(user.getId());
    }

    @PostMapping(value = "/register-employee")
    public ResponseEntity<?> registerEmployee(@Valid @RequestBody UserForm form) {
        if (userService.exists(form.getUsername())) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(Constants.ERROR_USER_EXISTS);
        }
        User user = userService.addUser(form.getUsername(), form.getPassword(), Role.TRAINER);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(user.getId());
    }

    @PostMapping("/generate-token")
    public ResponseEntity<?> generateToken(@Valid @RequestBody UserForm form) {
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        if (adminLogin != null && adminLogin.equalsIgnoreCase(authentication.getName())) {
            UserDetails adminUser = (UserDetails) authentication.getPrincipal();
            return ResponseEntity.status(HttpStatus.OK).body(jwtService.generateTokenAdmin(adminUser));
        } else {
            User authorizedUser = (User) authentication.getPrincipal();
            return ResponseEntity.status(HttpStatus.OK).body(jwtService.generateToken(authorizedUser));
        }
    }
}
