package pl.dmcs.bbiu.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dmcs.bbiu.back.form.ScheduleForm;
import pl.dmcs.bbiu.back.model.Schedule;
import pl.dmcs.bbiu.back.model.User;
import pl.dmcs.bbiu.back.service.ActivityService;
import pl.dmcs.bbiu.back.service.RoomService;
import pl.dmcs.bbiu.back.service.ScheduleService;
import pl.dmcs.bbiu.back.service.UserService;
import pl.dmcs.bbiu.back.wrapper.ActivityWrapper;
import pl.dmcs.bbiu.back.wrapper.ScheduleWrapper;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@RestController
@RequestMapping("/api")
public class ScheduleController {

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private RoomService roomService;

    @GetMapping("/schedule")
    public ResponseEntity<?> get(Principal principal) {
        List<Schedule> scheduleList = scheduleService.get();
        if (scheduleList.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Optional<User> user;
        if (principal != null) {
            user = userService.get(principal.getName());
        } else {
            user = Optional.empty();
        }
        return ResponseEntity.status(HttpStatus.OK).body(ScheduleWrapper.prepareRespone(scheduleList, user));
    }

    @PostMapping("/schedule")
    public ResponseEntity<?> add(@Valid @RequestBody ScheduleForm form) {
        return ResponseEntity.status(HttpStatus.OK).body(scheduleService.add(form));
    }

    @GetMapping("/schedule/activities")
    public ResponseEntity<?> getActivities() {
        return ResponseEntity.status(HttpStatus.OK).body(ActivityWrapper.wrap(activityService.get()));
    }

    @GetMapping("/schedule/rooms")
    public ResponseEntity<?> getRooms() {
        return ResponseEntity.status(HttpStatus.OK).body(roomService.get());
    }

    @GetMapping("/schedule/signin")
    public ResponseEntity<?> signIn(Principal principal, @RequestParam String scheduleId) {
        return ResponseEntity.status(HttpStatus.OK).body(scheduleService.signIn(principal.getName(), Long.valueOf(scheduleId)));
    }

    @GetMapping("/schedule/signout")
    public ResponseEntity<?> signOut(Principal principal, @RequestParam String scheduleId) {
        return ResponseEntity.status(HttpStatus.OK).body(scheduleService.signOut(principal.getName(), Long.valueOf(scheduleId)));
    }
}
