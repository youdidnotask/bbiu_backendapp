package pl.dmcs.bbiu.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dmcs.bbiu.back.form.ActivityForm;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.service.UserServiceImpl;
import pl.dmcs.bbiu.back.wrapper.UserWrapper;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by Joanna Olejnik
 * 12/07/2020
 */
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Value("${app.admin.login}")
    private String adminLogin;

    @GetMapping("/employees")
    public ResponseEntity<?> getEmployees() {
        return ResponseEntity.status(HttpStatus.OK).body(this.userService.getUsers(Role.TRAINER));
    }

    @GetMapping("/clients")
    public ResponseEntity<?> getClients() {
        return ResponseEntity.status(HttpStatus.OK).body(this.userService.getUsers(Role.CLIENT));
    }

    @GetMapping("/account")
    public ResponseEntity<?> getUserDetails(Principal principal, @RequestParam Long userId) {
        UserWrapper userWrapper = userService.getUserDetails(userId);
        if (principal.getName().equals(adminLogin) || principal.getName().equals(userWrapper.email)) {
            if (userWrapper != null) {
                return ResponseEntity.status(HttpStatus.OK).body(userWrapper);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @PostMapping("/employees/activity")
    public ResponseEntity<?> assignActivity(@Valid @RequestBody ActivityForm form) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.assignActivity(form.getUserId(), form.getActivityId()));
    }
}
