package pl.dmcs.bbiu.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.dmcs.bbiu.back.service.UserService;

/**
 * Created by Joanna Olejnik
 * 18/06/2020
 */
@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    private UserService userService;

    @GetMapping("/setup")
    public ResponseEntity setTestData() {
        userService.createTestUsers();
        return new ResponseEntity(HttpStatus.OK);
    }
}
