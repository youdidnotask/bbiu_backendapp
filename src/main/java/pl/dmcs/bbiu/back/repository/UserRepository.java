package pl.dmcs.bbiu.back.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.bbiu.back.model.Role;
import pl.dmcs.bbiu.back.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 20/06/2020
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

    @EntityGraph(value = "User.Details")
    Optional<User> findById(Long id);

    List<User> findAllByAuthoritiesRole(Role role);
}
