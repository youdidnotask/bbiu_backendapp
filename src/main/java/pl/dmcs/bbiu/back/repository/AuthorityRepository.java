package pl.dmcs.bbiu.back.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.bbiu.back.model.Authority;
import pl.dmcs.bbiu.back.model.Role;

import java.util.Optional;

/**
 * Created by Joanna Olejnik
 * 21/06/2020
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    @EntityGraph(value = "Authority.Users")
    Optional<Authority> findByRole(Role role);
}
