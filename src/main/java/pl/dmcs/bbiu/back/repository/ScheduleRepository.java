package pl.dmcs.bbiu.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.bbiu.back.model.Schedule;

import java.util.List;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    List<Schedule> findAllByOrderByDayAscTimeAsc();
}
