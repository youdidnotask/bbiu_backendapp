package pl.dmcs.bbiu.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.bbiu.back.model.Room;

/**
 * Created by Joanna Olejnik
 * 11/07/2020
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}
